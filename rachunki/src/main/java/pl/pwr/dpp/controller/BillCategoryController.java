package pl.pwr.dpp.controller;

import com.google.common.collect.Lists;
import pl.pwr.dpp.entity.BillCategory;
import pl.pwr.dpp.service.BillCategoryService;

import java.util.List;

/**
 * This class exports methods to manage a bill categories
 */
public class BillCategoryController {
    private BillCategoryService billCategoryService = new BillCategoryService();

    /**
     * Creates a new category, and validates if it's actually exists.
     * @param categoryName name of new category
     */
    public void createCategory(String categoryName) {
        BillCategory newBillCategory = new BillCategory(categoryName);
        validateCategoryExists(categoryName);
        billCategoryService.createCategory(newBillCategory);
    }

    /**
     * Checks if category exists and returns it's id number
     * @param categoryName name of category
     * @return category id
     */
    public Integer getCategoryId(String categoryName) {
        Integer categoryId;
        validateCategoryIdExists(categoryName);
        return billCategoryService.getCategoryId(categoryName);
    }

    /**
     * List all of categories
     * @return List of all categories
     */
    public List<String> getAllCategories() {
        List<String> categories = Lists.newArrayList();
        List<BillCategory> categoryList = billCategoryService.findAll();
        for (BillCategory bills : categoryList) {
            categories.add(bills.getCategoryName());
        }
        return categories;
    }

    /**
     * Validates if category exists by it's name
     * @param categoryName name of category
     */
    private void validateCategoryExists(String categoryName) {
        List<BillCategory> categoryList = billCategoryService.findAll();
        for (BillCategory categories : categoryList)
            if (categories.getCategoryName().equalsIgnoreCase(categoryName)) {
                throw new RuntimeException("Podana kategoria już istnieje!");
            }
    }

    /**
     * Validates if category exists by it's id
     * @param categoryName name of category
     */
    private void validateCategoryIdExists(String categoryName) {
        List<BillCategory> categoryList = billCategoryService.findAll();
        for (BillCategory categories : categoryList)
            if (categories.getCategoryName().equalsIgnoreCase(categoryName)) {
                return;
            }
        throw new RuntimeException("Podana kategoria nie istnieje");
    }
}
