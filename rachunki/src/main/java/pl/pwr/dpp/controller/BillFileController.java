package pl.pwr.dpp.controller;

import org.apache.commons.lang3.StringUtils;
import pl.pwr.dpp.entity.BillFile;
import pl.pwr.dpp.service.BillFileService;

import java.io.File;
import java.util.List;
import java.util.NoSuchElementException;

public class BillFileController {
    private BillFileService billFileService = new BillFileService();

    /**
     * Creates a bill file object representing a file of a phisical bill
     *
     * @param name       name of file
     * @param fileFormat file format
     * @param file       a file
     */
    public void createBillFile(String name, String fileFormat, File file) {
        BillFile newBillFile = new BillFile(name, fileFormat, file);
        billFileValidation(newBillFile);
        createBillFileValidation(newBillFile);
        billFileService.createBillFile(newBillFile);
    }

    /**
     * Updates a bill file object
     *
     * @param id         identity number of file
     * @param name       name of file
     * @param fileFormat file format
     * @param file       a file
     */
    public void updateBillFile(Integer id, String name, String fileFormat, File file) {
        BillFile newBillFile = new BillFile(id, name, fileFormat, file);
        billFileValidation(newBillFile);
        updateBillFileValidation(newBillFile.getId(), id);
        billFileService.updateBillFile(id, newBillFile);
    }

    /**
     * Deletes file by it's id
     *
     * @param id identity number of file
     */
    public void deleteBillFile(Integer id) {
        billIdValidation(id);
        billFileService.deleteBillFile(id);
    }

    /**
     * Finds bill file by it's id number
     *
     * @param id identity number of bill file
     * @return found bill file
     */
    public BillFile findById(Integer id) {
        billIdValidation(id);
        return billFileService.findById(id);
    }

    public Integer findByName(String fileName) {
        billNameValidation(fileName);
        return billFileService.findByFileName(fileName);
    }

    /**
     * Finds all bill files
     *
     * @return List of bill files
     */
    public List<BillFile> findAll() {
        return billFileService.findAll();
    }

    /**
     * Validates file parameters
     *
     * @param newBillFile a bill file object
     */
    private void billFileValidation(BillFile newBillFile) {
        if (StringUtils.isEmpty(newBillFile.getName())) {
            throw new IllegalArgumentException("Pole nazwa nie może być puste");
        }
        if (StringUtils.isEmpty(newBillFile.getFileFormat())) {
            throw new IllegalArgumentException(("Pole format pliku nie może być puste"));
        }
        if (newBillFile.getFile() == null) {
            throw new NoSuchElementException("Plik nie znaleziony");
        }
    }

    /**
     * Validates if bill file already exists
     *
     * @param newBillFile a bill file object
     */
    private void createBillFileValidation(BillFile newBillFile) {
        List<BillFile> billFileList = billFileService.findAll();
        for (BillFile billFile : billFileList) {
            if (billFile.getName().equalsIgnoreCase(newBillFile.getName())) {
                if (billFile.getFileFormat().equalsIgnoreCase(newBillFile.getFileFormat())) {
                    throw new RuntimeException("Plik o takiej nazwie już istnieje");
                }
            }
        }
    }

    /**
     * Validates if id of bill file is already in database
     *
     * @param oldBillId identity number of old Bill File
     * @param billId    identity number of actual Bill File
     */
    private void updateBillFileValidation(Integer oldBillId, Integer billId) {
        if (billId.equals(oldBillId)) {
            throw new AssertionError("Id edytowanego pliku nie jest takie samo jak id istniejącego w bazie danych!");
        }
    }

    /**
     * Validates identity number of bill file object
     *
     * @param id identity number of bill file
     */
    private void billIdValidation(Integer id) {
        if (id == null) {
            throw new NullPointerException("Pole id nie może być puste");
        }
    }

    private void billNameValidation(String fileName) {
        if (fileName == null) {
            throw new NullPointerException();
        }
    }
}
