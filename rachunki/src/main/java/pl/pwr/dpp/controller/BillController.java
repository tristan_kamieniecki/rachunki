package pl.pwr.dpp.controller;

import pl.pwr.dpp.entity.Bill;
import pl.pwr.dpp.service.BillService;

import java.util.List;

public class BillController {
    BillService billService = new BillService();

    /**
     * Creates a bill object
     *
     * @param bill bill object
     */
    public void createBill(Bill bill) {
        billValidation(bill);
        billService.createBill(bill);
    }

    /**
     * Updates a bill inforamtion
     *
     * @param oldBillId identity number of bill we edit
     * @param newBill   new bill
     */
    public void updateBill(Integer oldBillId, Bill newBill) {
        billValidation(newBill);
        billUpdateValidation(newBill.getId(), oldBillId);
        billService.updateBill(oldBillId, newBill);
    }

    /**
     * Deletes a bill
     *
     * @param billId identity number of bill we delete
     */
    public void deleteBill(Integer billId) {
        billDeleteValidation(billId);
        billService.deleteBill(billId);
    }

    /**
     * Finds Bill by it's id
     *
     * @param billId identity number of bill
     * @return a bill object we are looking for
     */
    public Bill findBillById(Integer billId) {
        billIdValidation(billId);
        return billService.findBillById(billId);
    }

    /**
     * Finds Bill by it's name
     *
     * @param billName name of bill we are looking for
     * @return a bill object we are looking for
     */
    public Bill fillBillByName(String billName) {
        billNameValidation(billName);
        return billService.findBillByName(billName);
    }

    /**
     * Finds Bill by it's ncategory
     *
     * @param categoryId identity number of category that bill belongs
     * @return a List of Bills we are looking for
     */
    public List<Bill> findBillsByCategory(Integer categoryId) {
        billCategoryIdValidation(categoryId);
        return billService.findBillsByCategory(categoryId);
    }

    /**
     * Validates if the bill exists in application
     *
     * @param bill bill we check in our app
     */
    private void billValidation(Bill bill) {
        if (bill.isWarrantyNumber() == null) {
            throw new NullPointerException();
        }
        if (bill.getWarrantyTo() == null) {
            throw new NullPointerException("Pole gwarancji nie może być puste");
        }
        if (bill.getCreatedAt() == null) {
            throw new NullPointerException();
        }
        if (bill.getDescription() == null) {
            throw new NullPointerException();
        }
        if (bill.getFileId() == null) {
            throw new NullPointerException();
        }
        if (billService.findAll() != null) {
            for (Bill newBill : billService.findAll()) {
                if (newBill.getName().equalsIgnoreCase(bill.getName())) {
                    throw new AssertionError("Istnieje już rachunek o tej nazwie");
                }
            }
        }
    }

    /**
     * Updates information about id information in database
     *
     * @param billId    identity number of bill
     * @param oldBillId identity number of old bill
     */
    private void billUpdateValidation(Integer billId, Integer oldBillId) {
        if (!billId.equals(oldBillId)) {
            throw new RuntimeException("Id edytowanego rachunku nie jest takie samo jak id istniejącego w bazie danych!");
        }
    }

    private void billDeleteValidation(Integer billId) {
        if (billId == null) {
            throw new NullPointerException("Id rachunku nie może być puste");
        }
    }

    private void billNameValidation(String billName) {
        if (billName == null) {
            throw new IllegalStateException("Rachunek musi posiadać nazwę!");
        }
    }

    private void billIdValidation(Integer billId) {
        if (billId == null) {
            throw new NullPointerException("Pole id nie może być puste");
        }
    }

    private void billCategoryIdValidation(Integer categoryId) {
        if (categoryId == null) {
            throw new NullPointerException();
        }
    }
}
