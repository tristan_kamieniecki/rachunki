package pl.pwr.dpp;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.pwr.dpp.gui.MainPanel;

import java.sql.Connection;

public class App extends Application {
    Connection connection;
    Stage window;
    MainPanel mainPanel = new MainPanel();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        window = stage;
        window.setTitle("Rachunki 2.1");
        window.setScene(mainPanel.makeScene());
        window.show();
    }
}
