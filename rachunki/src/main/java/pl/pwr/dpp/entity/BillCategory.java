package pl.pwr.dpp.entity;

public class BillCategory {

    /**
     * This class represents bill categories in program
     */
    private Integer id;
    private String categoryName;

    /**
     *  Constructor making a new category by name
     * @param categoryName name of category
     */
    public BillCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Constructor making a new category by name and id
     * @param id identifical number of category
     * @param categoryName the name of category
     */
    public BillCategory(Integer id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    public Integer getId() {
        return id;
    }


    private void setId(Integer id) {
        this.id = id;
    }


    public String getCategoryName() {
        return categoryName;
    }


    private void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
