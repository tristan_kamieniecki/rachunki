package pl.pwr.dpp.entity;

import java.io.File;

/**
 * Class representing a file attached to bill
 */
public class BillFile {
    private Integer id;
    private String name;
    private String fileFormat;
    private File file;

    /**
     * Constructor making a bill file object by it's id, name file format and a file
     * @param id identity number of the bill
     * @param name name of the file
     * @param fileFormat file format
     * @param file a whole file
     */
    public BillFile(Integer id, String name, String fileFormat, File file) {
        this.id = id;
        this.name = name;
        this.fileFormat = fileFormat;
        this.file = file;
    }

    /**
     * Constructor making a bill file object by it's name file format and a file
     * @param name name of the file
     * @param fileFormat file format
     * @param file a whole file
     */
    public BillFile(String name, String fileFormat, File file) {
        this.id = 0;
        this.name = name;
        this.fileFormat = fileFormat;
        this.file = file;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public File getFile() {
        return file;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
