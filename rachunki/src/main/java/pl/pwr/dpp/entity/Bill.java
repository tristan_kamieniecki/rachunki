package pl.pwr.dpp.entity;

import java.time.LocalDate;

/**
 * Class Bill representing a digital form of physical bill.
 */
public class Bill {
    private Integer id;
    private String name;
    private String description;
    private Integer billCategory;
    private LocalDate createdAt;
    private boolean isWarranty;
    private LocalDate warrantyTo;
    private Integer fileId;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getBillCategory() {
        return billCategory;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public boolean isWarranty() {
        return isWarranty;
    }

    public Integer isWarrantyNumber() {
        if (isWarranty) return 1;
        else return 0;
    }


    public LocalDate getWarrantyTo() {
        return warrantyTo;
    }

    public Integer getFileId() {
        return fileId;
    }

    /**
     * This class builds a Build obcject used in application.
     */
    public static final class BillBuilder {
        private Integer id;
        private String name;
        private String description;
        private Integer billCategory;
        private LocalDate createdAt;
        private boolean isWarranty;
        private LocalDate warrantyTo;
        private Integer fileId;


        public static BillBuilder builder() {
            return new BillBuilder();
        }

        public BillBuilder setId(Integer id) {
            this.id = id;
            return this;
        }

        public BillBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public BillBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public BillBuilder setBillCategory(Integer billCategory) {
            this.billCategory = billCategory;
            return this;
        }

        public BillBuilder setCreatedAt(LocalDate createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        /**
         * This function changes the Warranty status
         * @param warranty menas if warranty ever existed
         * @return warranty existance
         */
        public BillBuilder setWarranty(Integer warranty) {
            if (warranty == 0)
                this.isWarranty = false;
            else
                this.isWarranty = true;
            return this;
        }

        public BillBuilder setWarrantyTo(LocalDate warrantyTo) {
            this.warrantyTo = warrantyTo;
            return this;
        }

        public BillBuilder setFileId(Integer fileId) {
            this.fileId = fileId;
            return this;
        }

        /**
         * This functon builds a Bill object with it's category, warranty status and loaded file.
         * @return digital form of bill object
         */
        public Bill build() {
            if (name.isEmpty()) {
                throw new IllegalStateException("Rachunek musi posiadać nazwę!");
            }
            if (billCategory == null) {
                throw new IllegalStateException("Rachunek musi mieć przydzieloną kategorie!");
            }
            if (fileId == null) {
                throw new IllegalStateException("Rachunek musi zawierać plik!");
            }

            Bill bill = new Bill();
            bill.id = this.id;
            bill.name = this.name;
            bill.description = this.description;
            bill.billCategory = this.billCategory;
            bill.createdAt = this.createdAt;
            bill.isWarranty = this.isWarranty;
            bill.warrantyTo = this.warrantyTo;
            bill.fileId = this.fileId;
            return bill;
        }
    }
}
