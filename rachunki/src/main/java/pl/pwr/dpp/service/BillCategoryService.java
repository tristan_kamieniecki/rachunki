package pl.pwr.dpp.service;

import com.google.common.collect.Lists;
import pl.pwr.dpp.db.SQLConnection;
import pl.pwr.dpp.entity.BillCategory;

import java.sql.*;
import java.util.List;

public class BillCategoryService {

    /**
     * Creates new bills category in database
     *
     * @param newBillCategory
     */
    public void createCategory(BillCategory newBillCategory) {
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "INSERT INTO bill_categories(category_name) VALUES (?)";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, newBillCategory.getCategoryName());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets category Id from database
     *
     * @param categoryName name of category
     * @return id of category
     */
    public Integer getCategoryId(String categoryName) {
        Integer categoryId = null;
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "SELECT category_id from bill_categories WHERE category_name = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, categoryName);
            ResultSet rs = statement.executeQuery();
            categoryId = rs.getInt("category_id");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categoryId;
    }

    /**
     * Finds all bill categories in database
     *
     * @return return list of all categories
     */
    public List<BillCategory> findAll() {
        List<BillCategory> categories = Lists.newArrayList();
        try {
            Connection connection = SQLConnection.getConnection();
            Statement statement = connection.createStatement();

            String query = "SELECT category_id,category_name FROM bill_categories";

            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                categories.add(new BillCategory(rs.getInt("category_id"), rs.getString("category_name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }
}
