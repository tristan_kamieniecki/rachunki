package pl.pwr.dpp.service;

import com.google.common.collect.Lists;
import pl.pwr.dpp.db.SQLConnection;
import pl.pwr.dpp.entity.BillFile;

import java.io.*;
import java.sql.*;
import java.util.List;

public class BillFileService {

    /**
     * Creates bill file in database
     *
     * @param newBillFile new bill file
     */
    public void createBillFile(BillFile newBillFile) {
        try {
            Connection connection = SQLConnection.getConnection();
            int s = 0;


            String query = "INSERT INTO files(file_name, file_format, file) VALUES (?, ?, ?)";
            File file = newBillFile.getFile();
            FileInputStream input = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            for (int readNum; (readNum = input.read(buf)) != -1; ) {
                bos.write(buf, 0, readNum);
            }
            byte[] person_image = null;
            person_image = bos.toByteArray();


            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, newBillFile.getName());
            statement.setString(2, newBillFile.getFileFormat());
            statement.setBytes(3, person_image);
            s = statement.executeUpdate();

            if (s > 0) {
                System.out.println("Image Uploaded");
            }

        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates bill's file information in database
     *
     * @param id       id of bill
     * @param billFile a file of bill
     */
    public void updateBillFile(Integer id, BillFile billFile) {
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "UPDATE files SET file_name = ?, file_format = ?, file = ? WHERE file_id = ?";
            FileInputStream input = new FileInputStream(billFile.getFile());

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, billFile.getName());
            statement.setString(2, billFile.getFileFormat());
            statement.setBinaryStream(3, input);
            statement.setInt(4, id);
            statement.executeUpdate();

        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Deletes bill file from database
     *
     * @param id of file to delete
     */
    public void deleteBillFile(Integer id) {
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "DELETE FROM files WHERE file_id = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Finds file by it's id
     *
     * @param id id of file
     * @return a bill file
     */
    public BillFile findById(Integer id) {
        BillFile billFile = null;
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "SELECT * FROM files WHERE file_id = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);

            ResultSet rs = statement.executeQuery();

            billFile = new BillFile(rs.getInt("file_id"),
                    rs.getString("file_name"),
                    rs.getString("file_format"),
                    getFileFromBlob(rs));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return billFile;
    }

    public Integer findByFileName(String fileName) {
        Integer billFileId = null;
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "SELECT file_id FROM files WHERE file_name = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, fileName);

            ResultSet rs = statement.executeQuery();

            billFileId = rs.getInt("file_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return billFileId;
    }

    /**
     * Finds all bill files in database
     *
     * @return List of all files
     */
    public List<BillFile> findAll() {
        List<BillFile> files = Lists.newArrayList();
        try {
            Connection connection = SQLConnection.getConnection();
            Statement statement = connection.createStatement();

            String query = "SELECT * FROM files";

            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                files.add(
                        new BillFile(
                                rs.getInt("file_id"),
                                rs.getString("file_name"),
                                rs.getString("file_format"),
                                getFileFromBlob(rs)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return files;
    }

    /**
     * Gets file by result set
     *
     * @param resultSet set of files
     * @return exact file
     */
    private File getFileFromBlob(ResultSet resultSet) {
        File file = null;
        FileOutputStream fileOutputStream = null;
        try {
            if (!resultSet.getString("file_name").isEmpty()) {
                file = new File("data/" + resultSet.getString("file_name"));
                fileOutputStream = new FileOutputStream(file);
            } else throw new IllegalStateException();
            if (resultSet.next()) {
                InputStream fileInputStream = resultSet.getBinaryStream("file");
                byte[] buffer = new byte[1024];
                while (fileInputStream.read(buffer) > 0) {
                    fileOutputStream.write(buffer);
                }
            }

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return file;
    }
}
