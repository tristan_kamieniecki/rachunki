package pl.pwr.dpp.service;

import com.google.common.collect.Lists;
import pl.pwr.dpp.db.SQLConnection;
import pl.pwr.dpp.entity.Bill;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class BillService {

    /**
     * Creating bill in database
     *
     * @param bill object of bill we want to create in database
     */
    public void createBill(Bill bill) {
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "INSERT INTO bills (name, description, category_id, created_at, is_warranty, warranty_to, bill_file_id)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, bill.getName());
            statement.setString(2, bill.getDescription());
            statement.setInt(3, bill.getBillCategory());
            statement.setString(4, bill.getCreatedAt().toString());
            statement.setInt(5, bill.isWarrantyNumber());
            statement.setString(6, bill.getWarrantyTo().toString());
            statement.setInt(7, bill.getFileId());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updated bill in database by it's id
     *
     * @param id   id of bill we want to change in database
     * @param bill bill with new values we want to have
     */
    public void updateBill(Integer id, Bill bill) {
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "UPDATE bills SET name = ?, description = ?, category_id = ?, created_at = ?, " +
                    "is_warranty = ?, warranty_to = ?, bill_file_id = ? WHERE id = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, bill.getName());
            statement.setString(2, bill.getDescription());
            statement.setInt(3, bill.getBillCategory());
            statement.setString(4, bill.getCreatedAt().toString());
            statement.setInt(5, bill.isWarrantyNumber());
            statement.setString(6, bill.getWarrantyTo().toString());
            statement.setInt(7, bill.getFileId());
            statement.setInt(8, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes bill in database
     *
     * @param id id of bill we delete
     */
    public void deleteBill(Integer id) {
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "DELETE FROM bills WHERE id = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Finds bill in database by it's id
     *
     * @param id id of bill we are looking for in database
     * @return a bill
     */
    public Bill findBillById(Integer id) {
        Bill bill = null;
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "SELECT * FROM bills WHERE id = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate createdAt = LocalDate.parse(rs.getString("created_at"), formatter);
            LocalDate warrantyTo = LocalDate.parse(rs.getString("warranty_to"), formatter);


            bill = Bill.BillBuilder.builder()
                    .setId(id)
                    .setName(rs.getString("name"))
                    .setDescription((rs.getString("description")))
                    .setBillCategory(rs.getInt("category_id"))
                    .setCreatedAt(createdAt)
                    .setWarranty(rs.getInt("is_warranty"))
                    .setWarrantyTo(warrantyTo)
                    .setFileId(rs.getInt("bill_file_id"))
                    .build();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bill;
    }

    /**
     * Finds bill in database by it's name
     *
     * @param name id of bill we are looking for in database
     * @return a bill
     */
    public Bill findBillByName(String name) {
        Bill bill = null;
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "SELECT * FROM bills WHERE name = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate createdAt = LocalDate.parse(rs.getString("created_at"), formatter);
            LocalDate warrantyTo = LocalDate.parse(rs.getString("warranty_to"), formatter);


            bill = Bill.BillBuilder.builder()
                    .setId(rs.getInt("id"))
                    .setName(name)
                    .setDescription((rs.getString("description")))
                    .setBillCategory(rs.getInt("category_id"))
                    .setCreatedAt(createdAt)
                    .setWarranty(rs.getInt("is_warranty"))
                    .setWarrantyTo(warrantyTo)
                    .setFileId(rs.getInt("bill_file_id"))
                    .build();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bill;
    }

    /**
     * Finds bill in database by it's category id
     *
     * @param categoryId id of category that the bill we are looking for in database belongs
     * @return a bill
     */
    public List<Bill> findBillsByCategory(Integer categoryId) {
        List<Bill> bill = Lists.newArrayList();
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "SELECT * FROM bills WHERE category_id = ?";

            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, categoryId);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate createdAt = LocalDate.parse(rs.getString("created_at"), formatter);
                LocalDate warrantyTo = LocalDate.parse(rs.getString("warranty_to"), formatter);


                bill.add(Bill.BillBuilder.builder()
                        .setId(rs.getInt("id"))
                        .setName(rs.getString("name"))
                        .setDescription((rs.getString("description")))
                        .setBillCategory(rs.getInt("category_id"))
                        .setCreatedAt(createdAt)
                        .setWarranty(rs.getInt("is_warranty"))
                        .setWarrantyTo(warrantyTo)
                        .setFileId(rs.getInt("bill_file_id"))
                        .build());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bill;
    }

    /**
     * Finds all of the bills in database
     *
     * @return a list of bills from database
     */
    public List<Bill> findAll() {
        List<Bill> bill = Lists.newArrayList();
        try {
            Connection connection = SQLConnection.getConnection();

            String query = "SELECT * FROM bills";

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate createdAt = LocalDate.parse(rs.getString("created_at"), formatter);
                LocalDate warrantyTo = LocalDate.parse(rs.getString("warranty_to"), formatter);


                bill.add(Bill.BillBuilder.builder()
                        .setId(rs.getInt("id"))
                        .setName(rs.getString("name"))
                        .setDescription((rs.getString("description")))
                        .setBillCategory(rs.getInt("category_id"))
                        .setCreatedAt(createdAt)
                        .setWarranty(rs.getInt("is_warranty"))
                        .setWarrantyTo(warrantyTo)
                        .setFileId(rs.getInt("bill_file_id"))
                        .build());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bill;
    }
}

