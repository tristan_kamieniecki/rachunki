package pl.pwr.dpp.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.pwr.dpp.controller.BillCategoryController;

public class AddCategoryBox {
    public static void display(String title) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        window.setResizable(false);
        BillCategoryController billCategoryController = new BillCategoryController();

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(20,20,20,40));

        VBox addCategoryBox = new VBox(10);
        addCategoryBox.setAlignment(Pos.CENTER);
        Label categoryNameLabel = new Label("Nazwa kategorii: ");
        GridPane.setConstraints(categoryNameLabel, 1, 1);
        TextField categoryInput = new TextField();
        GridPane.setConstraints(categoryInput, 1, 1);
        addCategoryBox.getChildren().addAll(categoryNameLabel, categoryInput);

        Button buttonAddCategory = new Button("Dodaj kategorię");
        buttonAddCategory.setOnAction(e -> {
            billCategoryController.createCategory(categoryInput.getText());
            window.close();
        });

        addCategoryBox.getChildren().addAll(buttonAddCategory);
        gridPane.getChildren().addAll(addCategoryBox);
        Scene scene = new Scene(gridPane, 200, 150);
        window.setScene(scene);
        window.showAndWait();
    }
}
