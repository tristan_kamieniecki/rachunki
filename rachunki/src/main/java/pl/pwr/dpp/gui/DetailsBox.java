package pl.pwr.dpp.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.pwr.dpp.controller.BillController;
import pl.pwr.dpp.controller.BillFileController;
import pl.pwr.dpp.entity.Bill;
import pl.pwr.dpp.entity.BillFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DetailsBox {


    public static void display(String title, String itemName) {
        BillController billController = new BillController();
        BillFileController fileController = new BillFileController();
        Bill bill = billController.fillBillByName(itemName);


        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        window.setResizable(false);

        VBox listContainer = new VBox(20);
        listContainer.setPadding(new Insets(20, 20, 20, 20));

        HBox nameBox = new HBox(10);
        nameBox.setAlignment(Pos.CENTER);
        Label nameLabel = new Label("Nazwa paragonu: ");
        Label name = new Label();
        name.setText(bill.getName());
        nameBox.getChildren().addAll(nameLabel, name);

        HBox descriptionBox = new HBox(10);
        descriptionBox.setAlignment(Pos.CENTER);
        Label descriptionLabel = new Label("Opis: ");
        Label description = new Label();
        description.setText(bill.getDescription());
        descriptionBox.getChildren().addAll(descriptionLabel, description);

        HBox dateBox = new HBox(10);
        dateBox.setAlignment(Pos.CENTER);
        Label dateLabel = new Label("Data zakupu: ");
        Label date = new Label();
        date.setText(bill.getCreatedAt().toString());
        dateBox.getChildren().addAll(dateLabel, date);

        Button buttonShowPicture = new Button("Pokaż zdjęcie");
        buttonShowPicture.setOnAction(e -> {
            Integer fileId = bill.getFileId();
            BillFile billFile = fileController.findById(fileId);
            File file = billFile.getFile();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save file");
            File destinationFile = fileChooser.showSaveDialog(window);
            if (destinationFile != null) {
                try {
                    Files.copy(file.toPath(), destinationFile.toPath());
                } catch (IOException ex) {
                    throw new RuntimeException("Coś poszło nie tak!");
                }
            }
        });
        Button buttonClose = new Button("Powrót");
        buttonClose.setOnAction(e -> window.close());

        listContainer.getChildren().addAll(nameBox, descriptionBox, dateBox, buttonShowPicture, buttonClose);

        Scene scene = new Scene(listContainer, 200, 300);
        window.setScene(scene);
        window.showAndWait();
    }
}
