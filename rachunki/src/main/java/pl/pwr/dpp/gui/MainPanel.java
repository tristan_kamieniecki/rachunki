package pl.pwr.dpp.gui;

import com.google.common.collect.Lists;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.pwr.dpp.controller.BillCategoryController;
import pl.pwr.dpp.controller.BillController;
import pl.pwr.dpp.controller.BillFileController;
import pl.pwr.dpp.entity.Bill;

import java.io.File;
import java.util.List;

public class MainPanel {
    private BillController controller = new BillController();
    private BillCategoryController categoryController = new BillCategoryController();
    private BillFileController fileController = new BillFileController();
    private Bill.BillBuilder billBuilder;
    private String tempName;
    private File file = null;
    private String fileName = null;
    private String fileExtension = null;

    public Scene makeScene() {
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(20, 20, 20, 20));
        grid.setVgap(8);
        grid.setHgap(10);

        Label nameLabel = new Label("Nazwa paragonu:");
        GridPane.setConstraints(nameLabel, 1, 0);

        TextField nameInput = new TextField();
        GridPane.setConstraints(nameInput, 1, 1);

        Label descriptionLabel = new Label("Opis:");
        GridPane.setConstraints(descriptionLabel, 1, 2);

        TextField descriptionInput = new TextField();
        GridPane.setConstraints(descriptionInput, 1, 3);

        Label categoryLabel = new Label("Wybierz kategorię:");
        GridPane.setConstraints(categoryLabel, 1, 4);

        Label calendarLabel = new Label("Wybierz datę zakupu:");
        GridPane.setConstraints(calendarLabel, 1, 6);

        DatePicker datePicker = new DatePicker();
        GridPane.setConstraints(datePicker, 1, 7);

        Label warrantyLabel = new Label("Czy produkt jest objęty gwarancją:");
        GridPane.setConstraints(warrantyLabel, 1, 8);

        HBox varrantyContainer = new HBox(10);
        CheckBox varrantyCheckBox = new CheckBox("Tak");

        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        choiceBox.getItems().add("1 rok");
        choiceBox.getItems().add("2 lata");
        choiceBox.getItems().add("3 lata");
        choiceBox.getItems().add("4 lata");
        choiceBox.getItems().add("5 lat");

        varrantyContainer.getChildren().addAll(varrantyCheckBox, choiceBox);
        GridPane.setConstraints(varrantyContainer, 1, 9);


        HBox acceptContainer = new HBox(10);
        Button fileButton = new Button("Załącz zdjęcie");
        Button acceptButton = new Button("Zapisz rachunek");
        acceptButton.setVisible(true);
        acceptButton.setDisable(true);

        GridPane.setConstraints(acceptButton, 2, 9);
        acceptContainer.getChildren().addAll(fileButton);

        GridPane.setConstraints(acceptContainer, 1, 10);


        TreeItem<String> root, electronics, furniture;
        root = new TreeItem<>();
        root.setExpanded(false);


        List<TreeItem<String>> nodes = Lists.newArrayList();
        List<String> categories = categoryController.getAllCategories();
        List<Bill> items = null;

        for (String category : categories) {
            nodes.add(new TreeItem<>());
        }
        for (String s : categories) {
            //Kategoria
            TreeItem<String> stringTreeItem;
            stringTreeItem = makeBranch(s, root);
            //Elementy
            List<Bill> billsByCategory = controller.findBillsByCategory(categoryController.getCategoryId(s));
            items = Lists.newArrayList();
            items.addAll(billsByCategory);
            for (Bill item : items) {
                makeBranch(item.getName(), stringTreeItem);
            }
        }

        TreeView<String> tree = new TreeView<>(root);
        tree.setShowRoot(false);


        tree.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            if (newValue != null) {
                tempName = newValue.getValue();
            }
        });

        ChoiceBox<String> categoryChoiceBox = new ChoiceBox<>();
        GridPane.setConstraints(categoryChoiceBox, 1, 5);
        for (String category : categories) {
            categoryChoiceBox.getItems().add(category);
        }


        fileButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Wybierz zdjęcie rachunku");
            fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Tylko pdf lub zdjęcie",
                    ".pdf", ".jpg", ".png", ".gif"));
            file = fileChooser.showOpenDialog(new Stage());
            fileName = file.getName().split("\\.")[0];
            fileExtension = file.getName().split("\\.")[1];
            acceptButton.setDisable(false);
        });

        acceptButton.setOnAction(event -> {
            fileController.createBillFile(fileName, fileExtension, file);   //UPLOAD PLIKU
            Integer isWarranty;
            if (varrantyCheckBox.isSelected()) {
                isWarranty = 1;
            } else {
                isWarranty = 0;
            }
            Bill bill = Bill.BillBuilder.builder()
                    .setName(nameInput.getText())
                    .setDescription(descriptionInput.getText())
                    .setBillCategory(categoryController.getCategoryId(categoryChoiceBox.getValue()))
                    .setCreatedAt(datePicker.getValue())
                    .setWarranty(isWarranty)
                    .setWarrantyTo(datePicker.getValue().plusYears(getYear(choiceBox.getValue())))
                    .setFileId(fileController.findByName(fileName))
                    .build();
            controller.createBill(bill);    //UPLOAD RACHUNKU
        });

        Button detailsButton = new Button("Pokaż szczegóły");
        GridPane.setConstraints(detailsButton, 0, 10);
        detailsButton.setOnAction(event -> DetailsBox.display("Szczegóły", tempName));

        Button addBillButton = new Button("Dodaj rachunek");
        GridPane.setConstraints(addBillButton, 0, 11);

        Button addCategoryButton = new Button("Dodaj kategorię");
        GridPane.setConstraints(addCategoryButton, 0, 9);
        addCategoryButton.setOnAction(event -> AddCategoryBox.display("Dodawanie kategorii"));

        tree.setPrefHeight(270);

        GridPane.setConstraints(tree, 0, 0, 1, 9);
        grid.getChildren().addAll(nameInput, nameLabel, descriptionLabel, descriptionInput, categoryLabel, categoryChoiceBox, datePicker, acceptContainer,
                calendarLabel, warrantyLabel, varrantyContainer, acceptButton, detailsButton, addCategoryButton, tree);


        return new Scene(grid, 600, 400);
    }

    public TreeItem<String> makeBranch(String title, TreeItem<String> parent) {
        TreeItem<String> item = new TreeItem<>(title);
        item.setExpanded(true);
        parent.getChildren().add(item);
        return item;
    }

    public Integer getYear(String years) {
        Integer year = null;
        if (years.equalsIgnoreCase("1 rok")) {
            year = 1;
        } else if (years.equalsIgnoreCase("2 lata")) {
            year = 2;
        } else if (years.equalsIgnoreCase("3 lata")) {
            year = 3;
        } else if (years.equalsIgnoreCase("4 lata")) {
            year = 4;
        } else {
            year = 5;
        }
        return year;
    }
}
