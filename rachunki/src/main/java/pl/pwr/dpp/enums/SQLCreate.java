package pl.pwr.dpp.enums;

public enum SQLCreate {
    FOREIGN_KEY_ON("PRAGMA foreign_keys = ON;"),
    CREATE_TABLE_BILL("CREATE TABLE IF NOT EXISTS bills(" +
            " id INTEGER PRIMARY KEY AUTOINCREMENT," +
            " name TEXT NOT NULL," +
            " description TEXT," +
            " category_id INTEGER NOT NULL," +
            " created_at TEXT NOT NULL," +
            " is_warranty INTEGER NOT NULL," +
            " warranty_to TEXT," +
            " bill_file_id INTEGER NOT NULL," +

            " CONSTRAINT fk_file" +
            " FOREIGN KEY (bill_file_id) " +
            " REFERENCES files (file_id)" +
            " CONSTRAINT fk_category" +
            " FOREIGN KEY (category_id) " +
            " REFERENCES bill_categories (category_id)" +
            ");"),

    CREATE_TABLE_FILE("CREATE TABLE IF NOT EXISTS files(" +
            "file_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "file_name TEXT NOT NULL, " +
            "file_format TEXT NOT NULL, " +
            "file BLOB NOT NULL" +
            ");"),

    CREATE_TABLE_BILL_CATEGORY("CREATE TABLE IF NOT EXISTS bill_categories(" +
            " category_id INTEGER PRIMARY KEY AUTOINCREMENT," +
            " category_name TEXT NOT NULL" +
            ");"),

    DROP_TABLE_FILE("DROP TABLE IF EXISTS files;"),

    DROP_TABLE_CATEGORIES("DROP TABLE IF EXISTS bill_categories;"),

    DROP_TABLE_BILL("DROP TABLE IF EXISTS bills;");


    private String query;

    SQLCreate(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }
}
