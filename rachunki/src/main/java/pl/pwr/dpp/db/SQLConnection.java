package pl.pwr.dpp.db;

import pl.pwr.dpp.enums.SQLCreate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class SQLConnection {
    private static final String DB_URL = "jdbc:sqlite:Rachunki.db";

    private static Connection connection;

    public static Connection getConnection() {
        try {
            if (connection == null) {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection(DB_URL);
                createDatabase(connection);
                //createTestData(connection);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    private static void createDatabase(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(SQLCreate.CREATE_TABLE_BILL.getQuery());
        statement.execute(SQLCreate.CREATE_TABLE_FILE.getQuery());
        statement.execute(SQLCreate.CREATE_TABLE_BILL_CATEGORY.getQuery());
        statement.execute(SQLCreate.FOREIGN_KEY_ON.getQuery());
    }

    private static void createTestData(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        //  KATEGORIE PRODUKTÓW
        statement.execute("insert into bill_categories(category_name) values ('ELEKTRONIKA')");
        statement.execute("insert into bill_categories(category_name) values ('MECHANIKA')");
        statement.execute("insert into bill_categories(category_name) values ('RTV')");
        statement.execute("insert into bill_categories(category_name) values ('AGD')");
    }
}
