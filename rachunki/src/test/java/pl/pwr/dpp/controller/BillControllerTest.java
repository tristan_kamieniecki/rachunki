package pl.pwr.dpp.controller;

import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.pwr.dpp.entity.Bill;
import pl.pwr.dpp.service.BillService;

import java.time.LocalDate;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BillControllerTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @InjectMocks
    BillController billController;

    @Mock
    BillService billService;

    private static final Integer BILL_ID = 1;
    private static final Integer NOT_THE_SAME_BILL_ID = 0;
    private static final Integer FILE_ID = 1;

    @Test
    public void shouldThrowExceptionWhenWarrantyIsNull() {
        exception.expect(NullPointerException.class);
        exception.expectMessage("Pole gwarancji nie może być puste");
        when(billService.findAll()).thenReturn(Lists.newArrayList());

        billController.updateBill(BILL_ID, Bill.BillBuilder.builder().setId(NOT_THE_SAME_BILL_ID).setName("asd").setBillCategory(1).setFileId(FILE_ID).build());
    }

    @Test
    public void shouldThrowExceptionWhenDeleteBillIdIsNull() {
        exception.expect(NullPointerException.class);
        exception.expectMessage("Id rachunku nie może być puste");

        billController.deleteBill(null);
    }

    @Test
    public void shouldThrowExceptionWhenOldIdIsDifferentThanNewId() {
        exception.expect(RuntimeException.class);
        exception.expectMessage("Id edytowanego rachunku nie jest takie samo jak id istniejącego w bazie danych!");

        billController.updateBill(BILL_ID, Bill.BillBuilder.builder()
                .setId(NOT_THE_SAME_BILL_ID)
                .setName("asd")
                .setBillCategory(1)
                .setFileId(FILE_ID)
                .setWarrantyTo(LocalDate.now())
                .setCreatedAt(LocalDate.now())
                .setDescription("asd")
                .setFileId(FILE_ID)
                .build());
    }


    @Test
    public void shouldThrowExceptionWhenBillNameIsNull() {
        exception.expect(IllegalStateException.class);
        exception.expectMessage("Rachunek musi posiadać nazwę!");
        when(billService.findAll()).thenReturn(Lists.newArrayList());

        billController.updateBill(FILE_ID,
                Bill.BillBuilder.builder()
                        .setId(NOT_THE_SAME_BILL_ID)
                        .setName("")
                        .setBillCategory(1)
                        .setFileId(FILE_ID)
                        .setWarrantyTo(LocalDate.now())
                        .setCreatedAt(LocalDate.now())
                        .setDescription("asd")
                        .setFileId(FILE_ID)
                        .build());
    }

    @Test
    public void shouldThrowExceptionWhenFindFileIdIsNull() {
        exception.expect(NullPointerException.class);
        exception.expectMessage("Pole id nie może być puste");

        billController.findBillById(null);
    }
}