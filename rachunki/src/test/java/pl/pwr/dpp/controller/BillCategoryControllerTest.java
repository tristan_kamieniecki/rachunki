package pl.pwr.dpp.controller;

import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.pwr.dpp.entity.BillCategory;
import pl.pwr.dpp.service.BillCategoryService;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BillCategoryControllerTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @InjectMocks
    BillCategoryController billCategoryController;

    @Mock
    BillCategoryService billCategoryService;

    private static final String EXISTS_CATEGORY = "category";


    @Test
    public void shouldThrowExceptionWhenCategoryAlreadyExists() {
        exception.expect(RuntimeException.class);
        exception.expectMessage("Podana kategoria już istnieje!");

        when(billCategoryService.findAll()).thenReturn(Lists.newArrayList(new BillCategory(EXISTS_CATEGORY)));

        billCategoryController.createCategory(EXISTS_CATEGORY);
    }
}