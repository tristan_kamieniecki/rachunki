package pl.pwr.dpp.controller;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.pwr.dpp.service.BillFileService;

import java.io.File;
import java.util.NoSuchElementException;

@RunWith(MockitoJUnitRunner.class)
public class BillFileControllerTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @InjectMocks
    BillFileController billFileController;

    @Mock
    BillFileService billFileService;

    private static final Integer FILE_ID = 1;
    private static final String EMPTY_NAME = "";
    private static final String FILE_NAME = "file_name";
    private static final String EMPTY_FILE_FORMAT = "";
    private static final String FILE_FORMAT = "file_format";
    private static final File EMPTY_FILE = null;

    @Test
    public void shouldThrowExceptionWhenUpdateBillFileFileIsNull() {
        exception.expect(NoSuchElementException.class);
        exception.expectMessage("Plik nie znaleziony");

        billFileController.updateBillFile(FILE_ID, FILE_NAME, FILE_FORMAT, EMPTY_FILE);
    }

    @Test
    public void shouldThrowExceptionWhenUpdateBillFileFormatIsEmpty() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Pole format pliku nie może być puste");

        billFileController.updateBillFile(FILE_ID, FILE_NAME, EMPTY_FILE_FORMAT, EMPTY_FILE);
    }

    @Test
    public void shouldThrowExceptionWhenUpdateBillFileNameIsEmpty() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Pole nazwa nie może być puste");

        billFileController.updateBillFile(FILE_ID, EMPTY_NAME, FILE_FORMAT, EMPTY_FILE);
    }

    @Test
    public void shouldThrowExceptionWhenDeleteFileIdIsNull() {
        exception.expect(NullPointerException.class);
        exception.expectMessage("Pole id nie może być puste");

        billFileController.deleteBillFile(null);
    }

    @Test
    public void shouldThrowExceptionWhenFindFileIdIsNull() {
        exception.expect(NullPointerException.class);
        exception.expectMessage("Pole id nie może być puste");

        billFileController.findById(null);
    }
}